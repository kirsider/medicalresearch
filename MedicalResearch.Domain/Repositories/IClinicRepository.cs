﻿using System;
using System.Collections.Generic;
using System.Text;
using MedicalResearch.Domain.Models;

namespace MedicalResearch.Domain.Repositories
{
    public interface IClinicRepository
    {
        Clinic GetClinicById(int clinicId);
        IEnumerable<Clinic> GetClinics();
        void AddClinic(Clinic clinic);
        void EditClinic(Clinic clinic);
    }
}
