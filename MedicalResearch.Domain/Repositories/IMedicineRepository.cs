﻿using System;
using System.Collections.Generic;
using System.Text;
using MedicalResearch.Domain.Models;

namespace MedicalResearch.Domain.Repositories
{
    public interface IMedicineRepository
    {
        Medicine GetMedicineById(int medicineId);
        IEnumerable<Medicine> GetMedicines();
        void AddMedicine(Medicine medicine);
        void EditMedicine(Medicine medicine);
        void RemoveMedicine(int medicineId);
    }
}
