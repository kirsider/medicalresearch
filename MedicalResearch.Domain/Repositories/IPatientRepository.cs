﻿using System;
using System.Collections.Generic;
using System.Text;
using MedicalResearch.Domain.Models;

namespace MedicalResearch.Domain.Repositories
{
    public interface IPatientRepository
    {
        Patient GetPatientById(string patientId);
        IEnumerable<Patient> GetPatients();
        void AddPatient(Patient patient);
    }
}
