﻿using System;

namespace MedicalResearch.Domain.Models
{
    public class Medicine
    {
        public int Id { get; set; }
        public string MedicineType { get; set; }
        public string Description { get; set; }
        public string DosageForm { get; set; }
        public string Container { get; set; }
        public MedicineState MedicineState { get; set; }
        public DateTime ExpireAt { get; set; }
        public int TotalAmount { get; set; }
    }

    public enum MedicineState
    {
        Ok,
        Damaged,
        Lost
    }
}
