﻿using System;

namespace MedicalResearch.Domain.Models
{
    public class Patient
    {
        public string Id { get; set; }
        public Sex Sex { get; set; }
        public DateTime DateOfBirth { get; set; }
        public Status Status { get; set; }
        public string MedicineType { get; set; }
        public DateTime? LastVisit { get; set; }
        public Clinic Clinic { get; set; }
    }

    public enum Sex
    {
        Male,
        Female
    }

    public enum Status
    {
        Screened,
        Randomized,
        Finished,
        FinishedEarly
    }

}
