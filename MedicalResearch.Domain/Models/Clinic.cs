﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MedicalResearch.Domain.Models
{
    public class Clinic
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string City { get; set; }
        public string Address { get; set; }
        public string Address2 { get; set; }
        public string Phone { get; set; }
    }
}
