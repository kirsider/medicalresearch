﻿using System;

namespace MedicalResearch.Domain.Models
{
    public class User
    {
        public int Id { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Initials { get; set; }
        public string Email { get; set; }
        public string PhotoPath { get; set; }

        public Role Role { get; set; }      
    }
}
