﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MedicalResearch.Domain.Models
{
    public class Supply
    {
        public int Id { get; set; } 
        public Clinic Clinic { get; set; }
        public Medicine Medicine { get; set; }
        public int Amount { get; set; }
    }
}
